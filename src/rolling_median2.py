from flask import Flask
import json
import csv
import os
from pprint import pprint
import requests
import time
from datetime import datetime
from json import JSONDecoder
import sys
import numpy as np


def to_uniq(seq):
	# not order preserving, delete replicates
	set = {}
	map(set.__setitem__, seq, [])
	return set.keys()


# def create_data_s(tweets):
# 	'''takes the parsed json and passes the elements in to array,
# 	which are then used in the graph data structure. returns
# 	edges(nump(nx3)), verticies(numpy(nx2))'''
# 	v = create_verticies_s(tweets)
# 	return create_edges_s([to_uniq(tweets[0]), tweets[1]]), v


# def create_verticies_s(tweet):
# 	'''creates a vertex for every hashtag in a tweet'''
# 	ts = tweet[1]
# 	tags = to_uniq(tweet[0])
# 	if len(tags) < 1:
# 		return np.array([ts])
# 	return np.append([np.array(tags)], [[ts]*len(tags)], 0).T


def find_arr(a, b):
	'''finds the indicies of repeated values between two arrays'''
	a = a[:, [0, 1]].tolist()
	b1 = b[:, [0, 1]].tolist()
	b2 = b[:, [1, 0]].tolist()
	ind_a = []
	ind_b = []
	for i in range(0, len(b)):
		if b1[i] in a:
			ind_a.append(a.index(b1[i]))
			ind_b.append(i)
		elif b2[i] in a:
			ind_a.append(a.index(b2[i]))
			ind_b.append(i)
	return ind_a, ind_b


# def create_edge_s(transaction):
# 	'''creates an array of edges each edge consists of 2
# 	verticies, and a time creates an edges between every
# 	hashtag in a tweet together'''
# 	ts = transaction[1]
# 	edge = transaction[0]
# 	for tag in tags:
# 		if len(tags) > 1:
# 			tags = np.delete(tags, np.where(tags == tag))
# 			e = np.array([[tag]] * len(tags))
# 			add = np.append([tags], [[ts]*len(tags)], 0).T
# 			e = np.append(e, add, 1)
# 			if len(edges) < 1:
# 				edges = e
# 			else:
# 				edges = np.append(edges, e, 0)
# 	return edges


class Graph(object):
	"""docstring for Graph, allows for the sliding window to be adjustable dt"""
	def __init__(self, dt=60):
		super(Graph, self).__init__()
		self.thresh = 0
		self.dt = dt
		# self.verticies = {}
		self.edges = {}

	# def avg_deg(self):
	# 	'''returns the average degree of a graph'''
	# 	if len(self.verticies) is 0:
	# 		return 0
	# 	return 2 * len(self.edges)/float(len(self.verticies))

	def delete_old(self):
		'''deletes all the edges and verticies whose threshold is lower
		than the graphs. called when a newer vertex is added
		and the threshold is updated'''
		# if ((len(self.verticies) > 1) & (len(self.edges) > 1)):
		# 	old_verts = np.where(self.verticies[:, 1] < str(self.thresh))
		# 	old_edges = np.where(self.edges[:, 2] < str(self.thresh))
		# 	self.verticies = np.delete(self.verticies, old_verts[0].tolist(), 0)
		# 	self.edges = np.delete(self.edges, old_edges[0].tolist(), 0)
		for v in self.edges:
			for i in range(0, len(v[1])):
				if v[1][i] < self.thresh:
					# delete edges
					# try to delete 2 at a time bidirectional
					del v[0][i]
					del v[1][i]

	# def del_old(dicta, thresh):
		# for v in dicta:
		# 	old_edges = []
		# 	for i in range(0, len(v[1])):
		# 		if v[1][i] < thresh:
		# 			old_edges.append(i)
		# 	del v[1][old_edges]
		# 	del v[0][old_edges]
		# 			# delete edges
		# 			# try to delete 2 at a time bidirectional



	
	def add_new(self, nodeA, nodeB, ts):
		'''adds a new list of verticies and edges to the graph'''
		print "NodeA: ", nodeA, " nodeB: ", nodeB, "ts:", ts
		if nodeA is None or nodeB is None or ts is None:
			raise ValueError("Something wrong with input")
		if float(ts) > self.thresh + self.dt:
			self.thresh = float(ts) - self.dt
			self.delete_old()
		'''determines which exist previously and updates their
		ts. adds non existant ones to G'''
		# if len(self.verticies) != 0:
		# Vexist_in_g = np.where(self.verticies[:, [0]] ==\
		# 				np.intersect1d(self.verticies[:, 0], v[:, 0]))[0]
		if nodeA in self.edges and nodeB in self.edges:
			#  if edge exists
			if nodeB in self.edges[nodeA][0]:
				#  check ts 
				if ts > self.edges[nodeB][1]:
					indA = self.edges[nodeB][0].index(nodeA)
					indB = self.edges[nodeA][0].index(nodeB)
					self.edges[nodeA][1][indB] = ts
					self.edges[nodeB][1][indA] = ts
		elif nodeA in self.edges:
			self.edges[nodeA][0].append(nodeB)
			self.edges[nodeA][1].append(ts)
			self.edges[nodeB] = [[nodeA], [ts]]
		elif nodeB in self.edges:
			self.edges[nodeB][0].append(nodeA)
			self.edges[nodeB][1].append(nodeB)
		else:
			self.edges[nodeA] = [[nodeB], [ts]]
			self.edges[nodeB] = [[nodeA], [ts]]


		# # Vexist_in_v = np.where(v[:, [0]] ==\
		# # 				np.intersect1d(self.verticies[:, 0], v[:, 0]))[0]
		# V_2_replace_g = np.where(self.verticies[Vexist_in_g, 1] <\
		# 				v[Vexist_in_v, 1])[0].tolist()
		# V_2_replace_v = np.where(v[Vexist_in_v, 1] >\
		# 				self.verticies[Vexist_in_g, 1])[0].tolist()

		# Vind_2_replace = Vexist_in_g[V_2_replace_g]
		# Vind_replacing = Vexist_in_v[V_2_replace_v]

		# self.verticies[Vind_2_replace, [1]] = v[Vind_replacing, [1]]
		# self.verticies = np.append(self.verticies, np.delete(v, Vexist_in_v, 0), 0)

		# if len(self.edges) != 0:
		# 	rep_g, rep_e = find_arr(self.edges, e)
		# 	E_2_replace_g = np.where(self.edges[rep_g, 2] < e[rep_e, 2])[0]
		# 	E_2_replace_e = np.where(e[rep_e, 2] > self.edges[rep_g, 2])[0]
		# 	rep_e = np.array(rep_e)
		# 	rep_g = np.array(rep_g)
		# 	Eind_2_replace = rep_g[E_2_replace_g]
		# 	Eind_replacing = rep_e[E_2_replace_e]
		# 	if len(rep_e) > 0:
		# 		self.edges[Eind_2_replace, [2]] = e[Eind_replacing, [2]]
		# 		self.edges = np.append(self.edges, np.delete(e, rep_e, 0), 0)
		# 	else:
		# 		self.edges = np.append(self.edges, e, 0)
		# else:
		# 	self.edges = e


def parse_transaction(json_obj):
	'''parses the tweet into an array with the 0th index
	being an aray of hashtags, the the 1th being the timestamp'''
	print "json_obj: ", json_obj
	if 'created_time' in json_obj:
		ct = json_obj["created_time"].replace("T", " ")
		ct = ct.replace("Z", "")
		ts = time.mktime(time.strptime(ct, "%Y-%m-%d %H:%M:%S"))
		nodeA = json_obj['target'].lower()
		nodeB = json_obj['actor'].lower()
		# txt = 'text'
		# return [[d[txt].lower() for d in json_obj['target']\
		# 		if txt in d], ts]
		return nodeA, nodeB, ts
	else:
		return None, None, None


def main(jsonfile, output_txt):
	if os.path.exists(jsonfile) is not True:
		raise Exception("No such file.")
	with open(jsonfile, 'r') as data_file:
		G = Graph()
		lines = data_file.readlines()
		mid_degree = []
		out = open(output_txt, 'w')
		i=1
		for line in lines:
			d = json.loads(line)
			nodeA, nodeB, ts = parse_transaction(d) # returns [a, b, ts]
			G.add_new(nodeA, nodeB, ts)
			out.write(str("%.2f" % G.avg_deg())+'\n') #TODO: get median funct
			i += 1


if __name__ == '__main__':
	input_txt = sys.argv[1]
	output_txt = sys.argv[2]
	main(input_txt, output_txt)

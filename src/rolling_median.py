from flask import Flask
import json
import csv
import os
from pprint import pprint
import requests
import time
from datetime import datetime
from json import JSONDecoder
import sys
import numpy as np


def find_arr(a, b):
	'''finds the indicies of repeated values between two arrays'''
	a = a[:, [0, 1]].tolist()
	b1 = b[:, [0, 1]].tolist()
	b2 = b[:, [1, 0]].tolist()
	ind_a = []
	ind_b = []
	for i in range(0, len(b)):
		if b1[i] in a:
			ind_a.append(a.index(b1[i]))
			ind_b.append(i)
		elif b2[i] in a:
			ind_a.append(a.index(b2[i]))
			ind_b.append(i)
	return ind_a, ind_b

class Graph(object):
	"""docstring for Graph, allows for the sliding window to be adjustable dt"""
	def __init__(self, dt=60):
		super(Graph, self).__init__()
		self.thresh = 0
		self.dt = dt
		# self.verticies = {}
		self.edges = {}
		self.tss = {}

	def med_degree(self):
		'''Determines the median degree of the graph'''
		degree = []
		if len(self.edges)<1:
			return 0
		for v in self.edges:
			degree.append(len(self.edges[v][0]))
		return np.median(degree)

	# def del_old(self):
	# 	# removes old edges one at a time
	# 	for v in self.edges:
	# 		for i in  range(len(self.edges[v][1])-1,-1,-1):
	# 			if self.edges[v][1][i] < self.thresh:	
	# 				del self.edges[v][1][i]
	# 				del self.edges[v][0][i]
	
	def del_old(self):
		'''removes the expired edges in the graph in groups acording to their most recent timestamp'''
		toDelete = []
		oldTs = []
		for ts in self.tss:
			if ts < self.thresh:
				toDelete.extend(self.tss[ts])
				oldTs.append(ts)
		for e in toDelete:
			tempA = self.edges[e[0]][0].index(e[1])
			tempB = self.edges[e[1]][0].index(e[0])
			del self.edges[e[0]][0][tempA]
			del self.edges[e[0]][1][tempA]
			del self.edges[e[1]][0][tempB]
			del self.edges[e[1]][1][tempB]
			if len(self.edges[e[0]][0])<1:
				del self.edges[e[0]]
			if len(self.edges[e[1]][0])<1:
				del self.edges[e[1]]
		for ts in oldTs:
			del self.tss[ts]



	def add_new(self, nodeA, nodeB, ts):
		'''adds a new list of verticies and edges to the graph'''
		if nodeB is None:
			if float(ts) > self.thresh + self.dt:
				self.thresh = float(ts) - self.dt
				self.del_old()
		elif float(ts) > self.thresh:
			if float(ts) > self.thresh + self.dt:
				self.thresh = float(ts) - self.dt
				self.del_old()
			if ts in self.tss:
				if (nodeA, nodeB) not in self.tss[ts] and (nodeB, nodeA) not in self.tss[ts]:
					self.tss[ts].append((nodeA, nodeB))
			else:
				self.tss[ts] = [(nodeA, nodeB)]
			'''determines which exist previously and updates their
			ts. adds non existant ones to G'''

			if nodeA in self.edges and nodeB in self.edges:
				if nodeB in self.edges[nodeA][0]:
					indA = self.edges[nodeB][0].index(nodeA)
					indB = self.edges[nodeA][0].index(nodeB)
					if ts > self.edges[nodeB][1][indA]:
						if (nodeA, nodeB) in self.tss[self.edges[nodeB][1][indA]]:
							indexTs = self.tss[self.edges[nodeB][1][indA]].index((nodeA, nodeB))
							self.tss[self.edges[nodeB][1][indA]].remove((nodeA, nodeB))
							# del self.tss[self.edges[nodeB][1][indA]][indexTs]
						else:
							indexTs = self.tss[self.edges[nodeB][1][indA]].index((nodeB, nodeA))
							self.tss[self.edges[nodeB][1][indA]].remove((nodeB, nodeA))
							# del self.tss[self.edges[nodeB][1][indA]][indexTs]
						# add to new
						self.edges[nodeA][1][indB] = ts
						self.edges[nodeB][1][indA] = ts
				else:
					self.edges[nodeA][0].append(nodeB)
					self.edges[nodeA][1].append(ts)
					self.edges[nodeB][0].append(nodeA)
					self.edges[nodeB][1].append(ts)
					# add to new tss
					if ts in self.tss:
						if (nodeA, nodeB) not in self.tss[ts] and (nodeB, nodeA) not in self.tss[ts]:
							self.tss[ts].append((nodeA, nodeB))
					else:
						self.tss[ts] = [(nodeA, nodeB)]
			elif nodeA in self.edges:
				self.edges[nodeA][0].append(nodeB)
				self.edges[nodeA][1].append(ts)
				self.edges[nodeB] = [[nodeA], [ts]]
			elif nodeB in self.edges:
				self.edges[nodeB][0].append(nodeA)
				self.edges[nodeB][1].append(ts)
				self.edges[nodeA] = [[nodeB], [ts]]
			else: 
				self.edges[nodeA] = [[nodeB], [ts]]
				self.edges[nodeB] = [[nodeA], [ts]]


def parse_transaction(json_obj):
	'''parses the tweet into an array with the 0th index
	being an aray of hashtags, the the 1th being the timestamp'''
	if 'created_time' in json_obj:
		ct = json_obj["created_time"].replace("T", " ")
		ct = ct.replace("Z", "")
		ts = time.mktime(time.strptime(ct, "%Y-%m-%d %H:%M:%S"))
		nodeA = json_obj['target'].lower()
		nodeB = json_obj['actor'].lower()
		return nodeA, nodeB, ts
	else:
		return None, None, None


def main(jsonfile, output_txt):
	if os.path.exists(jsonfile) is not True:
		raise Exception("No such file.")
	with open(jsonfile, 'r') as data_file:
		G = Graph()
		lines = data_file.readlines()
		mid_degree = []
		out = open(output_txt, 'w')
		i=1
		for line in lines:
			d = json.loads(line)
			nodeA, nodeB, ts = parse_transaction(d) # returns [a, b, ts]
			if nodeA!="" and nodeB!="":
				G.add_new(nodeA, nodeB, ts)
			else:
				G.add_new(None, None, ts)
			out.write(str("%.2f" % G.med_degree())+'\n') #TODO: get median funct
			i += 1
			# else:
			# 	raise ValueError("Blank Entry")

if __name__ == '__main__':
	input_txt = sys.argv[1]
	output_txt = sys.argv[2]
	main(input_txt, output_txt)
